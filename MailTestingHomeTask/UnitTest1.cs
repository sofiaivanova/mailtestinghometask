using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace MailTestingHomeTask
{
    public class Tests
    {
        private IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://mail.ukr.net/");
        }

        [Test]
        public void Test1()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            IWebElement login_input = driver.FindElement(By.Name("login"));
            login_input.SendKeys("testuser1.0");

            IWebElement password_input = driver.FindElement(By.XPath("//input[@type='password']"));
            password_input.SendKeys("12345678pas");

            driver.FindElement(By.XPath("//button[contains(@class, 'jY4tHruE')]")).Click();

            driver.FindElement(By.CssSelector(".compose")).Click();

            IWebElement input_field = driver.FindElement(By.Name("toFieldInput"));
            input_field.SendKeys("testuser2.0@ukr.net");

            IWebElement subject_field = driver.FindElement(By.CssSelector("[name*='subject']"));
            subject_field.SendKeys("Happy Holidays!");


            IWebElement email_text = driver.FindElement(By.XPath("//div[@id='mceu_33']"));
            email_text.Click();

            driver.SwitchTo().Frame(driver.FindElement(By.Id("mce_0_ifr")));
            
            IWebElement text_area = driver.FindElement(By.XPath("//*[@id='tinymce']/div"));
            text_area.SendKeys("Hello, Petro! Happy Holidays!");

            driver.SwitchTo().DefaultContent();

            
            driver.FindElement(By.CssSelector(".button.primary.send")).Click();

            IWebElement ads_block = driver.FindElement(By.CssSelector(".sendmsg__ads.show"));
            String actualVal = ads_block.GetCssValue("visibility");

            Assert.That(actualVal, Is.EqualTo("visible"));
        }
        [TearDown]

        public void Close_Browser()

        {
            driver.Quit();
        }
    }
}